import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppBarComponent } from './main_components/app-bar/app-bar.component';
import { SideBarComponent } from './main_components/side-bar/side-bar.component';
import { FaqModule } from './faq/faq.module';
import { HealthcheckComponent } from './main_components/healthcheck/healthcheck.component';

@NgModule({
  declarations: [
    AppComponent,
    AppBarComponent,
    SideBarComponent,
    HealthcheckComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FaqModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
