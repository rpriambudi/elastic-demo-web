import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { FaqSearchResult } from './../../models/faq.model';
import { FaqService } from './../../services/interfaces/faq-service.interface';
import { SimpleDialogComponent } from './../../../shared/components/simple-dialog/simple-dialog.component';
import { Popular } from '../../models/popular.model';

@Component({
  selector: 'app-faq-search',
  templateUrl: './faq-search.component.html',
  styleUrls: ['./faq-search.component.scss']
})
export class FaqSearchComponent implements OnInit {
  public faqs: Observable<FaqSearchResult>;
  public populars: Observable<Popular[]>;
  public isLoading: boolean = false;
  public query: string = '';

  constructor(
    @Inject('FaqService') private faqService: FaqService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.faqs = this.faqService.search()
    .pipe(
      map(data => {
        this.isLoading = false;
        return data;
      }),
      catchError(error => {
        this.handleError(error);
        throw error;
      })
    )
    
    this.populars = this.faqService.findPopularQuery()
    .pipe(
      catchError(error => {
        this.handleError(error);
        throw error;
      })
    )
  }

  onClickSearch(): void {
    this.faqs = this.faqService.search(this.query === '' ? null : this.query)
    .pipe(
      map(data => {
        this.isLoading = false;
        return data;
      }),
      catchError(error => {
        this.handleError(error);
        throw error;
      })
    )
  }

  onClickCategory(categoryName: string): void {
    this.faqs = this.faqService.searchByCategory(categoryName, this.query)
    .pipe(
      map(data => {
        this.isLoading = false;
        return data;
      }),
      catchError(error => {
        this.handleError(error);
        throw error;
      })
    )
  }

  private handleError(error) {
    this.isLoading = false;
    this.dialog.open(SimpleDialogComponent, {
      width: '300px',
      data: { message: error}
    });
  }

}
