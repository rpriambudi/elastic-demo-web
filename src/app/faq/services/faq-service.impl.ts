import { Inject, Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { FaqService } from './interfaces/faq-service.interface';
import { Faq, FaqSearchResult } from './../models/faq.model';
import { HttpService } from './../../shared/services/interfaces/http-service.interface';
import { environment } from './../../../environments/environment';
import { Popular } from '../models/popular.model';

@Injectable()
export class FaqServiceImpl implements FaqService {
    constructor(@Inject('HttpService') private httpService: HttpService) {}
    
    create(faq: Faq): Observable<any> {
        return this.httpService.post(`${environment.faqServiceUrl}/api/faqs`, faq)
        .pipe(
            catchError(error => {
                if (typeof error === 'string')
                    return throwError(error);
                return throwError(`Could not connect to server. ${error.message}`);
            })
        );
    }

    update(faqId: number, faq: Faq): Observable<any> {
        return this.httpService.post(`${environment.faqServiceUrl}/api/faqs/${faqId}`, faq)
        .pipe(
            catchError(error => {
                if (typeof error === 'string')
                    return throwError(error);
                return throwError(`Could not connect to server. ${error.message}`);
            })
        );
    }
    
    find(): Observable<Faq[]> {
        return this.httpService.get<Faq[]>(`${environment.faqServiceUrl}/api/faqs`)
        .pipe(
            catchError(error => {
                if (typeof error === 'string')
                    return throwError(error);
                return throwError(`Could not connect to server. ${error.message}`);
            })
        )
    }

    findById(faqId: number): Observable<Faq> {
        return this.httpService.get<Faq>(`${environment.faqServiceUrl}/api/faqs/${faqId}`)
        .pipe(
            catchError(error => {
                if (typeof error === 'string')
                    return throwError(error);
                return throwError(`Could not connect to server. ${error.message}`);
            })
        )
    }

    delete(faqId: number): Observable<any> {
        return this.httpService.delete(`${environment.faqServiceUrl}/api/faqs/${faqId}`)
        .pipe(
            catchError(error => {
                if (typeof error === 'string')
                    return throwError(error);
                return throwError(`Could not connect to server. ${error.message}`);
            })
        );
    }

    search(query?: string): Observable<FaqSearchResult> {
        let path: string = 'api/user/faqs/search';
        if (query) {
            path += `?query=${query}`
        }

        return this.httpService.get<FaqSearchResult>(`${environment.faqServiceUrl}/${path}`)
        .pipe(
            catchError(error => {
                if (typeof error === 'string')
                    return throwError(error);
                return throwError(`Could not connect to server. ${error.message}`);
            })
        );
    }

    searchByCategory(categoryName: string, query?: string): Observable<FaqSearchResult> {
        let path: string = `api/user/faqs/search/${categoryName}/category`;
        if (query) {
            path += `?query=${query}`
        }

        return this.httpService.get<FaqSearchResult>(`${environment.faqServiceUrl}/${path}`)
        .pipe(
            catchError(error => {
                if (typeof error === 'string')
                    return throwError(error);
                return throwError(`Could not connect to server. ${error.message}`);
            })
        );
    }

    findPopularQuery(): Observable<Popular[]> {
        let path: string = 'api/faqs/popular/search';

        return this.httpService.get<Popular[]>(`${environment.faqServiceUrl}/${path}`)
        .pipe(
            catchError(error => {
                if (typeof error === 'string')
                    return throwError(error);
                return throwError(`Could not connect to server. ${error.message}`);
            })
        )
    }
}