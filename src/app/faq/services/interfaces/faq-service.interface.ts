import { Faq, FaqSearchResult } from './../../models/faq.model';
import { Observable } from 'rxjs';
import { Popular } from '../../models/popular.model';

export interface FaqService {
    create(faq: Faq): Observable<any>;
    update(faqId: number, faq: Faq): Observable<any>;
    find(): Observable<Faq[]>;
    findById(faqId: number): Observable<Faq>;
    findPopularQuery(): Observable<Popular[]>;
    delete(faqId: number): Observable<any>;
    search(query?: string): Observable<FaqSearchResult>;
    searchByCategory(categoryName: string, query?: string): Observable<FaqSearchResult>;
}