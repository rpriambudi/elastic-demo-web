import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { SpinnerComponent } from './components/spinner/spinner.component';
import { SimpleDialogComponent } from './components/simple-dialog/simple-dialog.component';
import { HttpServiceImpl } from './services/http-service.impl';



@NgModule({
  declarations: [
    SpinnerComponent,
    SimpleDialogComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [
    {
      provide: 'HttpService',
      useClass: HttpServiceImpl
    }
  ],
  exports: [
    SpinnerComponent,
    SimpleDialogComponent
  ]
})
export class SharedModule { }
